# Contributor: Maxim Karasev <begs@disroot.org>
# Maintainer: Maxim Karasev <begs@disroot.org>
pkgname=hut
pkgver=0.0.0_git20220326
_commit=8d4806be303e2b6486439009486f28fa05fac655
pkgrel=0
pkgdesc="command-line tool for sr.ht"
url="https://sr.ht/~emersion/hut"
arch="all"
license="AGPL-3.0-only"
makedepends="go scdoc"
subpackages="$pkgname-doc
	$pkgname-zsh-completion
	$pkgname-bash-completion
	$pkgname-fish-completion"
source="$pkgname-$pkgver.tar.gz::https://git.sr.ht/~emersion/hut/archive/$_commit.tar.gz"
builddir="$srcdir/$pkgname-$_commit"
options="!check" # no test suite

build() {
	make all

	./hut completion zsh >$pkgname.zsh
	./hut completion bash >$pkgname.bash
	./hut completion fish >$pkgname.fish
}

package() {
	make DESTDIR="$pkgdir" PREFIX=/usr install

	install -Dm644 $pkgname.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_$pkgname

	install -Dm644 $pkgname.bash \
		"$pkgdir"/usr/share/bash-completion/completions/$pkgname.bash

	install -Dm644 $pkgname.fish \
		"$pkgdir"/usr/share/fish/completions/$pkgname.fish
}

sha512sums="
fe9465c62560a97f48b739b6a8ab7a2f2d34d7a77aaff6d6657fb1d6657074964f2ec9bdd734e4999f278b5c7c58782aaa5b92021ab7a60e6d524def0113b57a  hut-0.0.0_git20220326.tar.gz
"
