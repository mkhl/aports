# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>

pkgname=font-iosevka
pkgver=15.1.0
pkgrel=0
pkgdesc="Versatile typeface for code, from code."
url="https://typeof.net/Iosevka/"
arch="noarch"
options="!check" # no testsuite
license="OFL-1.1"
depends="fontconfig"
subpackages="
	$pkgname-base
	$pkgname-slab
	$pkgname-curly
	$pkgname-curly-slab:curly_slab
	$pkgname-aile
	$pkgname-etoile
	"
source="
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-slab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-slab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-aile-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-etoile-$pkgver.zip
	"

builddir="$srcdir"

package() {
	depends="
		$pkgname-base
		$pkgname-slab
		$pkgname-curly
		$pkgname-curly-slab
		$pkgname-aile
		$pkgname-etoile
	"

	mkdir -p "$pkgdir"/usr/share/fonts/TTC
	mv "$builddir"/*.ttc "$pkgdir"/usr/share/fonts/TTC
}

base() {
	pkgdesc="$pkgdesc (Iosevka)"
	amove usr/share/fonts/TTC/iosevka.ttc
}

slab() {
	pkgdesc="$pkgdesc (Iosevka Slab)"
	amove usr/share/fonts/TTC/iosevka-slab.ttc
}

curly() {
	pkgdesc="$pkgdesc (Iosevka Curly)"
	amove usr/share/fonts/TTC/iosevka-curly.ttc
}

curly_slab() {
	pkgdesc="$pkgdesc (Iosevka Curly Slab)"
	amove usr/share/fonts/TTC/iosevka-curly-slab.ttc
}

aile() {
	pkgdesc="$pkgdesc (Iosevka Aile)"
	amove usr/share/fonts/TTC/iosevka-aile.ttc
}

etoile() {
	pkgdesc="$pkgdesc (Iosevka Etoile)"
	amove usr/share/fonts/TTC/iosevka-etoile.ttc
}

sha512sums="
05b48aa504ec73bfa8110a28f145edb18afac3f4ac2071294c9c83f1d06c0fcc73022d3203697653422da72d0713f2309dfd22b4187cf455de31976a89b6692f  super-ttc-iosevka-15.1.0.zip
dcefb7c3a51eeb1513fffabf872eaf3ad620627c86ba81eeaaee340098297c1d253f3c6b9699d0d8a0fa34e27413202b68f0a5fd53cdf932d5150c4d47c90d48  super-ttc-iosevka-slab-15.1.0.zip
7828b53d989ba46da533867e1cf305cf1744daf68b18fa5319e083b2419e69c5e0f51e3be3e83f8bcb29c07460813a052442d05e7a7422a56316d6003c52309e  super-ttc-iosevka-curly-15.1.0.zip
f075143ee82f81ccbdd5655aaeaabec8f907f282025d019299aa2ed762a42d9721966e3b054088df1139d66f38e593d92ff3f9e0fa7328c46697899148c53f32  super-ttc-iosevka-curly-slab-15.1.0.zip
ef91938faaaed5f1966051a79c9552277427376f85b6d08641c5bd15bb6a4ba085a4420050d3c23407e6d5877ec241fef99f4f5301dc852ec6f2d231ece637a7  super-ttc-iosevka-aile-15.1.0.zip
764c759dca15ae73cc6e4fae0dcf76034fec10a3b07b2c3d9eb6e68718f4d1d981bb6a68def2881f12b8f6f6855c914e8924c65ddcf60e4630c57b68661d2c87  super-ttc-iosevka-etoile-15.1.0.zip
"
